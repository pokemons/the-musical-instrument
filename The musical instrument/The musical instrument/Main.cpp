#include <iostream>
#include <string>
#include <fstream>

using namespace std;

const int NumberInstrument = 14;
const int NumberQuestion = 18;

const string SuitableVariants = "�������� ���������� ����������� �����������: "; 
const string UnsuitableVariants = "�������� ���������� ����������� �����������: "; 
const string Ending = "������ �� ������ ������ �� ����� �����������, �� ������� ��������. =)" ; 

const string SymbolOfInstrumentNameString = "$";


void Welcome() 
{
	cout << "������������, ��� ������������ ��������� The musical instrument." << "\n";
	cout << "��� �������� ���������� ���� ���������� � ������������ �����������." << "\n";
	cout << "��� ����� ���������� �������� �� ��� ��������." << "\n";
	cout << "������ ����������� � ��������� ������: Yes, YES, yes, No, NO, no." << "\n \n \n";
}


string SearchOfString (string Number)
{
	ifstream File ("questions.txt");
	string FileString;
	int NumberSize;
    bool Condition;
  
    if (!File.is_open())
	{
        cout << "Error! File questions.txt is not found."<< "\n";
		return ("NULL");
	}

    else
	{   
		NumberSize = Number.length();

		while(!File.eof())
		{
			getline(File, FileString);
			Condition = 1;

			for(int i = 0; i < NumberSize; i++)
			{
			    if (Number[i] != FileString[i])
				{
					 Condition = 0;					 
				}				
			}

			if (Condition == 1)
			{
				return (FileString);
			}
		}
    }
}

void OutputQuestions(string FileString)
{
	int i = 0;

	while (FileString[i] != ' ')
	{
		i++;
	}
	
	
	while (FileString[i] != '?')
	{
		i++;
		cout << FileString[i];
	}

	cout << "  ";
}

void BallsDistribution (int *InstrumentBals, string FileString, int *PositionNumber, bool Condition)
{
	if (Condition == 1)
	{
		if (FileString[*PositionNumber] == '+')
		{
			*InstrumentBals = *InstrumentBals + 1;
		}
		else
		{
			*InstrumentBals = *InstrumentBals - 1;
		}
	}
	else
	{
		if (FileString[*PositionNumber] == '+')
		{
			*InstrumentBals = *InstrumentBals - 1;
		}
		else
		{
			*InstrumentBals = *InstrumentBals + 1;
		}
	}
	*PositionNumber = *PositionNumber + 1;
}

int QuestionEndPosition (string FileString)
{
	int i = 0;
	while (FileString[i] != '?')
	{
		i++;
	}
	i++;
	return (i);
}

bool Answer(string i)
{ 
    bool answer;

	start:

	if (i == "Yes" || i == "YES" || i == "yes" || i == "No" || i == "NO" || i == "no")
	{
		if (i == "Yes" || i == "yes" || i == "YES")
 	 	{ 
			answer = 1;
  		}
		else
  		{ 
			answer = 0;
  		} 
	}
	else
	{
		cout << "�� ����� �������� ������ ������, ���������� ������� ������:  ";
		cin >> i;
		goto start;
	}
    return (answer);
}

string TransferInStringForm (int i)
{
	char str[3];
	string HelpString;
	i++;
	sprintf(str,"%i",i);
	HelpString = str;
	return (HelpString);
}

struct StructType
{
	string InstrumentName[10];
	int Balls;
};

void StructFilling(StructType HelpStruct[NumberInstrument])
{
	string InstrumentNameString;
	InstrumentNameString = SearchOfString(SymbolOfInstrumentNameString);
	int k = 1;

	for (int i = 0; i < NumberInstrument; i++, k++)
	{
		for (int j = 0; InstrumentNameString[k] != ' '; j ++, k++)
		{
			if (InstrumentNameString[k] == '@')
			{
				break;
			}
			HelpStruct[i].InstrumentName[j] = InstrumentNameString[k];
		}
		HelpStruct[i].Balls = 0;
	}
}

void OutputOfSuitableAndUnsuitableInstruments (StructType HelpStruct[NumberInstrument]) 
{
	StructType HelpStruct2;

	for (int i = 0; i < NumberInstrument - 1; i++)
	{
		for (int j = i + 1; j < NumberInstrument; j++)
		{ 
			if (HelpStruct[i].Balls < HelpStruct[j].Balls)
			{
				HelpStruct2 = HelpStruct[i];
				HelpStruct[i] = HelpStruct[j];
				HelpStruct[j] = HelpStruct2;
			}
		}
	}

	cout << "\n \n \n" << SuitableVariants;

	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; HelpStruct[i].InstrumentName[j] != "\0"; j++)
		{
			cout << HelpStruct[i].InstrumentName[j];
		}

		if (i == 2)
		{
			cout << ". ";
		}
		else
		{
			cout << ", ";
		}

	}

	cout << "\n" << UnsuitableVariants;

	for (int i = NumberInstrument - 3; i < NumberInstrument; i++)
	{
		for (int j = 0; HelpStruct[i].InstrumentName[j] != "\0"; j++)
		{
			cout << HelpStruct[i].InstrumentName[j];
		}

		if (i == 13)
		{
			cout << ". ";
		}
		else
		{
			cout << ", ";
		}
	}

	cout << "\n" << Ending << "\n";
}



void main ()
{
	setlocale(0, "Rus");
	Welcome();

	StructType InstrumentBalls[NumberInstrument];
	StructFilling(InstrumentBalls);

	string MainFileString, StringFormatOfNumberQuestion, UserResponse;
	bool UserResponseInBoolFormat;
	int InstrumentPosition;

	for (int i = 0; i < NumberQuestion; i++)
	{
		StringFormatOfNumberQuestion = TransferInStringForm(i);
		MainFileString = SearchOfString(StringFormatOfNumberQuestion);
		
		OutputQuestions (MainFileString);
		cin >> UserResponse;
		UserResponseInBoolFormat = Answer(UserResponse);
		
		InstrumentPosition = QuestionEndPosition(MainFileString);
		for (int j = 0; j < NumberInstrument; j++)
		{
			BallsDistribution(&InstrumentBalls[j].Balls, MainFileString, &InstrumentPosition, UserResponseInBoolFormat);
		}
    }

	OutputOfSuitableAndUnsuitableInstruments(InstrumentBalls);
	system ("pause");
}